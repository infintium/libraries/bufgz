package bufgz

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"time"
)

type Writer struct {
	// b *bufio.Writer
	io.Writer
	b *bytes.Buffer
	g *gzip.Writer
	n int
}

func NewWriter(w io.Writer) io.WriteCloser {
	b := &Writer{Writer: w}

	// b.b = bufio.NewWriterSize(w, 8192)
	b.b = new(bytes.Buffer)

	b.g, _ = gzip.NewWriterLevel(b.b, gzip.BestCompression)

	return b
}

func (w *Writer) Write(p []byte) (nn int, err error) {
	n, err := w.g.Write(p)
	if err != nil {
		return n, err
	}

	w.n += n

	if w.n > 16383 {
		err = w.g.Close()
		if err != nil {
			return n, err
		}

		ratio := float32(w.n) / float32(w.b.Len())
		fmt.Println(time.Now(), "BufGZ: In", w.n, "bytes. Out", w.b.Len(), "bytes. Ratio", ratio)

		_, err = w.Writer.Write(w.b.Bytes())
		// err = w.b.Flush()
		if err != nil {
			return n, err
		}

		w.b.Reset()

		w.g.Reset(w.b)

		w.n = 0
	}

	return n, err
}

func (w *Writer) Close() error {
	// err := w.g.Close()
	// if err != nil {
	// 	return err
	// }

	// return w.b.Flush()
	return w.g.Close()
}

type Reader struct {
	io.Reader
	b   *bytes.Buffer
	g   *gzip.Reader
	err error
	n   int64
	m   int
}

func NewReader(r io.Reader) (*Reader, error) {
	var err error
	b := &Reader{Reader: r}

	b.b = new(bytes.Buffer)

	b.g, err = gzip.NewReader(r)

	return b, err
}

func (r *Reader) Read(p []byte) (n int, err error) {

	// If the internal buffer is empty, read the next gzip block
	if r.b.Len() == 0 && r.err == nil {
		r.g.Multistream(false)

		r.n, r.err = r.b.ReadFrom(r.g)
		if r.err == nil {
			// log.Println("Read bytes", r.n)
			r.m++

			r.err = r.g.Reset(r.Reader)
		}
	}

	n, err = r.b.Read(p)

	if err == io.EOF {
		return n, r.err
	}

	return n, err
}

func (r *Reader) Len() int {
	return r.m
}
